#run 'pip install Pillow' in cmd for PIL package
from PIL import Image, ImageOps
img = Image.open('bg2.jpg')
img_with_border = ImageOps.expand(img,border=10,fill='red')
img_with_border.save('imaged-with-border.jpg')
